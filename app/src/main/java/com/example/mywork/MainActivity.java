package com.example.mywork;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Fragment weixinFragment=new weixinFragment();
    private Fragment friendFragment=new friendFragment();
    private Fragment contactFragment=new contactFragment();
    private Fragment configFragment=new configFragment();

    private  FragmentManager fragmentManager;

    private LinearLayout linearLayout1,linearLayout2,linearLayout3,linearLayout4;
    private ImageView imageWeixin,imageFriend,imageContact,imageConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        linearLayout1=findViewById(R.id.LinearLayout_weixin);
        linearLayout2=findViewById(R.id.LinearLayout_firend);
        linearLayout3=findViewById(R.id.LinearLayout_contact);
        linearLayout4=findViewById(R.id.LinearLayout_config);

        imageWeixin=findViewById(R.id.imageView1);
        imageFriend=findViewById(R.id.imageView3);
        imageContact=findViewById(R.id.imageView2);
        imageConfig=findViewById(R.id.imageView4);

        linearLayout1.setOnClickListener(this);
        linearLayout2.setOnClickListener(this);
        linearLayout3.setOnClickListener(this);
        linearLayout4.setOnClickListener(this);

        initFragment();
    }

    private void initFragment(){
        fragmentManager=getFragmentManager();
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.add(R.id.id_content,weixinFragment);
        transaction.add(R.id.id_content,configFragment);
        transaction.add(R.id.id_content,contactFragment);
        transaction.add(R.id.id_content,friendFragment);
        transaction.commit();
    }

    private void hideFragment(FragmentTransaction transaction){

        transaction.hide(weixinFragment);
        transaction.hide(configFragment);
        transaction.hide(contactFragment);
        transaction.hide(friendFragment);

    }

    private void showfragment(int i){
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        hideFragment(transaction);
        switch(i){
            case 0:
                transaction.show(weixinFragment);
                imageWeixin.setImageResource(R.drawable.bottom_weixin1);
                break;
            case 1:
                transaction.show(contactFragment);
                imageContact.setImageResource(R.drawable.bottom_contact1);
                break;
            case 2:
                transaction.show(friendFragment);
                imageFriend.setImageResource(R.drawable.bottom_friend1);
                break;
            case 3:
                transaction.show(configFragment);
                imageConfig.setImageResource(R.drawable.bottom_config1);
                break;
            default:
                break;
        }
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        resetImg();
        switch (v.getId()) {
            case R.id.LinearLayout_weixin:
                showfragment(0);
                break;
            case R.id.LinearLayout_contact:
                showfragment(1);
                break;
            case R.id.LinearLayout_firend:
                showfragment(2);
                break;
            case R.id.LinearLayout_config:
                showfragment(3);
                break;
            default:
                break;

        }

    }
    public void resetImg(){
        imageWeixin.setImageResource(R.drawable.bottom_weixin);
        imageContact.setImageResource(R.drawable.bottom_contact);
        imageFriend.setImageResource(R.drawable.bottom_friend);
        imageConfig.setImageResource(R.drawable.bottom_config);
    }

}